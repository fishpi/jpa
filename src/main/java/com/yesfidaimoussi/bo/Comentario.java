package com.yesfidaimoussi.bo;


import javax.persistence.*;

@Entity
public class Comentario {

    @Id

    // para la generation automatica de id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String texto;
    private String autor;

    //tengo mis dudas si es un OneToOne
    @ManyToOne (fetch = FetchType.LAZY)

    //La columna que va a servir un poco de referencia en la base de datos
    @JoinColumn(name="noticia_titulo")
    private Noticia noticia;

    public Comentario() {
    }

    public Comentario(int id) {
        this.id = id;
    }

    public Comentario(String texto, String autor, Noticia noticia) {
        this.texto = texto;
        this.autor = autor;
        this.noticia = noticia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Noticia getNoticia() {
        return noticia;
    }

    public void setNoticia(Noticia noticia) {
        this.noticia = noticia;
    }
}
