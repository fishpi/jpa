package com.yesfidaimoussi.bo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Noticia implements Serializable {

    @Id
    private String titulo;
    private String autor;
    private Date fecha;

    //cascade significa cuando borro una noticia se borran todos los comentarios asociados
    @OneToMany(mappedBy = "noticia",cascade = CascadeType.ALL)
    private List<Comentario> comentarios = new ArrayList<Comentario>();

    public Noticia() {
    }

    public Noticia(String titulo) {
        this.titulo = titulo;
    }

    public Noticia(String titulo, String autor, Date fecha) {
        this.titulo = titulo;
        this.autor = autor;
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentario) {
        this.comentarios = comentario;
    }

    public void addComentario(Comentario comentario){
        this.comentarios.add(comentario);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Noticia noticia = (Noticia) o;
        return Objects.equals(titulo, noticia.titulo) &&
                Objects.equals(autor, noticia.autor) &&
                Objects.equals(fecha, noticia.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titulo, autor, fecha);
    }
}
