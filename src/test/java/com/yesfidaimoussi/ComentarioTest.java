package com.yesfidaimoussi;

import com.yesfidaimoussi.bo.Comentario;
import com.yesfidaimoussi.bo.Noticia;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ComentarioTest extends JPAUnitTest{

    @Test
    public void entityManagerFactoryOk(){
        assertNotNull(entityManagerFactory);
    }

    @Test
    public void entityManagerOk(){
        assertNotNull(entityManager);
    }

    @Test
    public void selecionarComentarioInicial(){
        Comentario comentario = entityManager.find(Comentario.class,1);
        assertEquals("Mustapha", comentario.getAutor());
    }

    @Test
    public void selecionarComentarioInicialConsulta(){
        int total = entityManager.createQuery("select c from Comentario c").getResultList().size();
        assertEquals(1, total);
    }

    @Test
    public void seleccionarComentarioDeNoticia(){
        Noticia noticia = entityManager.find(Noticia.class,"Java 10 ha salido");
        Comentario comentario = noticia.getComentarios().get(0);
        assertEquals("Mustapha",comentario.getAutor());

    }
}
