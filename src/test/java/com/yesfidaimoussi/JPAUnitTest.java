package com.yesfidaimoussi;

import static org.junit.Assert.assertTrue;

import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Unit test for simple App.
 */
public class JPAUnitTest
{

    static EntityManagerFactory entityManagerFactory;
    protected EntityManager entityManager;

    @BeforeClass
    public static void setupStatic(){
        Persistence.generateSchema("curso",null);
        entityManagerFactory = Persistence.createEntityManagerFactory("curso");
    }

    @Before
    public void setup(){
        entityManager = entityManagerFactory.createEntityManager();
    }

    @After
    public void tearDown(){
        entityManager.clear();
        entityManager.close();
    }

    @AfterClass
    public static void tearDownStatic(){
        entityManagerFactory.close();
    }
}
