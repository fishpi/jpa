package com.yesfidaimoussi;

import com.yesfidaimoussi.bo.Noticia;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class NoticiaTest extends JPAUnitTest{

    @Test
    public void entityManagerFactoryOk(){
        assertNotNull(entityManagerFactory);
    }

    @Test
    public void entityManagerOk(){
        assertNotNull(entityManager);
    }

    @Test
    public void selecionarNoticiaInicial(){
        Noticia noticia = entityManager.find(Noticia.class,"Java 10 ha salido");
        assertEquals("Bilal", noticia.getAutor());
    }
    @Test
    public void borrarNoticiaInicial(){
        Noticia noticia = entityManager.find(Noticia.class,"Java 10 ha salido");
        entityManager.getTransaction().begin();
        entityManager.remove(noticia);
        entityManager.getTransaction().commit();
        Noticia sinNoticia = entityManager.find(Noticia.class,"Java 10 ha salido");
        assertNull(sinNoticia);
    }

    @Test
    public void insertarNoticiaNueva(){
        entityManager.getTransaction().begin();
        Noticia noticia = new Noticia("Noticia Nueva", "Bilal", new Date());
        entityManager.persist(noticia);
        entityManager.getTransaction().commit();
        Noticia noticiaInsertada = entityManager.find(Noticia.class,"Noticia Nueva");
        assertEquals(noticia.getTitulo(),noticiaInsertada.getTitulo());

    }
}
